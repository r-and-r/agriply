# Agriply #

## Project Overview ##

Agriply is a simple solution that aims to **improve agricultural productivity and rural livelihoods** by **innovating the knowledge sharing experience** and** connecting the gap** between our experts, farmers and enthusiasts.

Agriply is a **question and answer platform** for experts, local farmers and homemakers, and people passionate about agriculture and livelihood. Anyone can ask and answer a question and browse through the growing information available in the site. 

The platform is designed as a **generic Q&A platform compatible to Internet.org standards** (thus it can be **used for other relevant sectors like health and transportation**) and is available in both **Tagalog and English language** to be more appropriate for the Filipinos.

Agriply will help with the
* development of best practices, 
* promote mutual learning between experts and workers, 
* spread information more efficiently, fast and to masses and 
* create more awareness about available solutions and innovations in agriculture. 

At the end of the day, agricultural workers can **make better decisions** in order to take advantage of market opportunities and manage continuous changes in their production systems while experts and enthusiasts also **learn and gather data** from the insights of the workers to improve their researches as well as help **empowerment of agricultural workers**.

## Problem ##

Despite the innovations brought about by experts and availability of agricultural and livelihood resources in the country, there always remains a lack of agricultural information sometimes most basic, related to seeds, farming practices, climate, diseases and pests, harvesting mechanisms, application of farm machinery, post-harvest strategies and proper marketing. These lack of information coupled with other factors may lead to loss of produce and ultimately, the smallholder farmers, homemakers and people with jobs alike (referred to as agricultural workers)  will suffer.

The agricultural workers’ source of knowledge are limited and closed to their location, asymmetrically distributed and preserved within a group, constantly reinforced by experiences and trial and error and adapting and usually not updated with the available innovations and best practices that they can apply to improve the way they do things.

## Features ##
* Any user (anonymous or registered) can ask and/or answer a question
* Answers can be upvoted to validate or get feedback on answers
* Available in both Tagalog and English Language
* Users are associated with “stock” points based on how valid or “true" their answers are
* Discovery of questions as well as answers is very easy as the site will display:
* * How to use the app
* * Realtime questions that are displayed as they come
* * Trending topics and tags
* * Top users
* * Searching via tags
* Users will be able to view their profile and see their points, questions they've asked and answers they gave.
* Users can also update their profile to receive alerts via SMS/email whenever their questions are answered