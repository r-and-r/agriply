/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.bootstrap.html
 */

var userLevels = require('../data/userLevels.json');
var questionTypes= require('../data/questionTypes.json');

module.exports.bootstrap = function(sailsCallback) {

  async.parallel([
    function(callback) {
      UserLevel.count(function(err, count) {
        if (err) return callback(err);
        
        if (count === 0) {
          userLevels.forEach(function(userLevel) {
            UserLevel.create(userLevel, function() {});          
          });
        }

        callback();
      });
    },

    function(callback) {
      QuestionType.count(function(err, count) {
        if (err) return callback(err);
        
        if (count === 0) {
          questionTypes.forEach(function(questionType) {
            QuestionType.create(questionType, function() {});          
          });
        }

        callback();
      });
    }
  ], function(err) {
    sailsCallback(err);
  });
};
