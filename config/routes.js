/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  'get /user/signup':  'User.getSignup',
  'get /user/login':  'User.getLogin',
  'get /user/logout':  'User.logout',
  'get /user/update':  'User.getUpdate',
  'get /user/preference':  'User.userLanding',
  'get /user/:id':  'User.get',
  'post /user/signup':  'User.signup',
  'post /user/login':  'User.login',
  'post /user/update':  'User.update',

  'get /':  'Question.index',
  'get /question/browse':  'Question.browse',
  'get /question':  'Question.addQuestion',
  'get /question/upvote':  'Question.upvote',
  'get /question/downvote':  'Question.downvote',
  'get /question/report':  'Question.report',
  'get /question/:id':  'Question.get',
  'post /question/post':  'Question.post',
  
  'get /about':  'Web.about',
  'get /advocacy':  'Web.advocacy',
  'get /language/:lang':  'Web.localize',
  

  'post /answer/post':  'Answer.post',
  'get /answer/upvote':  'Answer.upvote',
  'get /answer/downvote':  'Answer.downvote',
  'get /answer/report':  'Answer.report',

};
