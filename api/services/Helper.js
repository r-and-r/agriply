var moment = require('moment');
var request = require('request');
var uuid = require('uuid');

var TAG = '[Helper.js]';

module.exports = {
  listQuestions: function(where, pagination, sort, callback) {
    var uid = where.uid;
    delete where.uid;

    Question.count({
      where: where
    }, function(err, count) {
      if (err) return res.serverError();
    
      Question.find({
        where: where
      })
      .paginate(pagination)
      .sort(sort)
      .populate('user')
      .exec(function(err, tmpQuestions) {
        if (err) return callback(err);
        
        var questions = [];
        var recentTags = [];
        async.each(tmpQuestions, function(question, eachCallback) {

          async.auto({

            answerCount: function(autoCallback) {
              Answer.count({
                question: question.id,
              }, autoCallback);
            },
            
            commentCount: function(autoCallback) {
              Comment.count({
                parentId: question.id,
                parentType: 'question'
              }, autoCallback);
            },

            votes: function(autoCallback) {
              Vote.sum({
                parentId: question.id,
                parentType: 'question'
              }, autoCallback);
            },

            reported: function(autoCallback) {
              Report.findOne({
                parentId: question.id,
                parentType: 'question',
                user: uid
              }, function(err, report) {
                autoCallback(err, report !== undefined);
              });
            },

          }, function(err, result) {
            if (err) return eachCallback(err);

            question.answerCount = result.answerCount;
            question.commentCount = result.commentCount;
            question.reported = result.reported;
            question.votes = result.votes;
            question.tags = question.tags.split(',');
            question.createdAt = moment(question.createdAt).fromNow();

            recentTags.push.apply(recentTags, question.tags);
            questions.push(question);
            eachCallback();
          });
          
        }, function(err) {
          recentTags = _.first(_.uniq(recentTags), 6);
          callback(err, questions, recentTags, count);
        });
      });
    });

  },
  sendSMS: function(mobile, message, callback){
    request({
      method: 'POST',
      url: sails.config.sms.baseUrl,
      form: {
        message_type: "SEND",
        mobile_number: mobile,
        shortcode: sails.config.sms.shortcode,
        message_id: Utils.MD5(uuid.v4()),
        message: message,
        client_id: sails.config.sms.clientId,
        secret_key: sails.config.sms.secretKey
      }
    }, function(error, response, body){
      if(error) {
        sails.log.error(TAG, error);
        callback();
      } else if(body.status == 200) {
        sails.log.debug(TAG, 'Success', JSON.stringify(body));
        callback();
      } else {
        sails.log.error(TAG, 'Non-200 Response', JSON.stringify(body));
        callback();
      }
    });
  }
};
