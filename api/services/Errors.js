module.exports = {
  generate: function(obj) {
    var error;
    switch(obj) {
      case 'update_phone_profile':
        error = 'Update your phone number to receive SMS alerts.';
        break;
      case 'update_phone_signup':
        error = 'Sign-up to receive SMS alerts.';
        break;
      case 'login':
        error = 'Error trying to log-in. Make sure username and password match.';
        break;
      case 'signup':
        error = 'Error trying to sign-up. Make sure all required fields are complete.';
        break;
      case 'question_invalid_arguments':
        error = "Error trying to add question. Make sure all required fields are complete.";
        break;
      default:
        error = 'stub';
    }
    return error;
  }
};
