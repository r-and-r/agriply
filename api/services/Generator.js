module.exports = {
  generateUser: function(password, callback) {
    var userPrefix = sails.config.generator.user.prefix;
    var rand = this.generateRandomInt(sails.config.generator.user.high, sails.config.generator.user.low);

    var username = userPrefix + password;

    User.findOne({ username: username }, function(err, user) {
      if (err) return callback(err);
      if (user) return callback('user_exists');

      User.create({
        username: username,
        password: Utils.MD5(password),
        userLevel: 3
      }, function(err, user) {
        if (err) return callback(err);

        callback(null, user);
      });
    });
    
  },

  generateRandomInt: function(high, low) {
    return Math.floor(Math.random() * (high - low) + low);
  }
};
