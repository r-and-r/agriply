/**
 * sessionAuth
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */
module.exports = function(req, res, next) {

  if (req.session.user) {
    next();
  } else {
    User.findOne({
      password: Utils.MD5(req.sessionID),
      userLevel: 3
    }, function(err, user) {
      if (err) return res.serverError(err);
      if (!user) {
        Generator.generateUser(req.sessionID, function(err, user) {
          if (err) return res.serverError(err);
          req.session.user = user;
          req.session.save(function() {
            next();
          });
        });
      } else {
        req.session.user = user;
        req.session.save(function() {
          next();
        });
      }
    });
  }
};
