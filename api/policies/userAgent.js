module.exports = function(req, res, next) {
  var useragent = req.get('user-agent');

  req.layout = 'layout';
  if (useragent.match(/Opera Mini/i)) {
    req.layout = 'm/layout';
  }

  next();
};
