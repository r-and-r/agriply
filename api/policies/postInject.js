module.exports = function(req, res, next) {
  if (req.method === 'POST') {
    req.body.user = req.session.user.id;
  }

  next();
};
