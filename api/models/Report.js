module.exports = {
  tableName: 'reports',
  
  attributes: {
    parentId: {
      type: 'integer',
      required: true
    },

    parentType: {
      type: 'string',
      required: true
    },

    user: {
      model: 'user'
    }
  },

};
