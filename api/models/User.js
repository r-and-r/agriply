module.exports = {
  tableName: 'users',

  attributes: {
    username: {
      type: 'string',
      required: true,
      unique: true
    },

    firstName: {
      type: 'string'
    },

    lastName: {
      type: 'string'
    },

    occupation: {
      type: 'string'
    }, 

    password: {
      type: 'string',
      required: true
    },

    phone: {
      type: 'string'
    },

    email: {
      type: 'string'
    },

    province: {
      type: 'string'
    },

    userLevel: {
      model: 'userlevel' 
    },

    questions: {
      collection: 'question',
      via: 'user'
    },

    answers: {
      collection: 'answer',
      via: 'user'
    },

    votes: {
      collection: 'vote',
      via: 'user'
    },

    comments: {
      collection: 'comment',
      via: 'user'
    }
  },

  login: function(username, password, callback) {
    User.findOne({
      username: username,
      password: Utils.MD5(password)
    }, function(err, user) {
      if (err) return callback(err);
      if (!user) return callback('invalid');

      callback(null, user);
    });
  },

  register: function(regObj, callback) {
    if (regObj.password !== regObj.confirmPassword) return callback('invalid_confirm');
    User.create({
      username: regObj.username,
      password: Utils.MD5(regObj.password),
      occupation: regObj.occupation,
      province: regObj.province,
      phone: regObj.phone,
      userLevel: 2
    }, callback);
  }
};
