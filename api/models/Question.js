module.exports = {
  tableName: 'questions',

  attributes: {
    title: {
      type: 'string',
      required: true
    },

    content: {
      type: 'string',
      required: true
    },

    questionType: {
      model: 'questiontype'
    },

    tags: {
      type: 'string',
      required: true
    },

    user: {
      model: 'user'
    },

    answers: {
      collection: 'answer',
      via: 'question'
    },
    
    alerts: {
      type: 'string'
    }
  }
};
