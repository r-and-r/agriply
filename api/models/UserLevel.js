module.exports = {
  tableName: 'user_levels',
  autoCreatedAt: false,
  autoUpdatedAt: false,
  autoPK: false,

  attributes: {
    id: {
      type: 'integer',
      required: true,
      primaryKey: true,
      unique: true
    },

    description: {
      type: 'string',
      required: true
    }
  }
};
