module.exports = {
  tableName: 'question_types',
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes: {
    id: {
      type: 'integer',
      required: true,
      primaryKey: true,
      unique: true
    },

    description: {
      type: 'string',
      required: true
    }
  }
};
