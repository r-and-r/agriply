module.exports = {
  tableName: 'votes',
  
  attributes: {
    parentId: {
      type: 'integer',
      required: true
    },

    parentType: {
      type: 'string',
      required: true
    },

    user: {
      model: 'user'
    },

    value: {
      type: 'integer',
      defaultsTo: 0
    }
  },

  sum: function(query, callback) {
    Vote.find(query, function(err, votes) {
      if (err) return callback(err);

      var voteList = _.map(votes, function(v) { return v.value });
      var voteCount = _.reduce(voteList, function(vote, count) {
        return vote + count;
      }, 0);

      callback(null, voteCount);
    });  
  },

  up: function(query, callback) {
    doVote(query, 1, callback);
  },

  down: function(query, callback) {
    doVote(query, -1, callback);
  }
};

function doVote(query, value, callback) {
  Vote.findOne(query, function(err, vote) {
    if (err) return callback(err);

    if (vote) {
      vote.value = value;
      vote.save(callback);
    } else {
      query.value = value;
      Vote.create(query, callback);
    }
  });
}
