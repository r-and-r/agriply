module.exports = {
  tableName: 'comments',
  
  attributes: {
    parentId: {
      type: 'integer',
      required: true
    },

    parentType: {
      type: 'string',
      required: true
    },

    user: {
      model: 'user'
    },

    content: {
      type: 'string',
      required: true
    }
  }
};
