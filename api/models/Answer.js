module.exports = {
  tableName: 'answers',

  attributes: {
    content: {
      type: 'string',
      required: true
    },

    user: {
      model: 'user'
    },

    question: {
      model: 'question'
    },

    accepted: {
      type: 'boolean',
      defaultsTo: false
    }
  }
};
