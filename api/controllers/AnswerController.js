module.exports = {
  post: function(req, res) {
    Question.findOne({ id: req.body.question }).populate('user').exec(function(err, question){
      Answer.create(req.body, function(err, answer) {
        if (err) return res.serverError();
        if(question.alerts === 'on' && question.user.phone){
          var message = "Hello " + question.user.username + ", someone answered your question: " + question.title + "\n\nVisit http://agriply.rr.voyager.ph/question/" + question.id  + " to view. Thanks!"; 
          Helper.sendSMS(question.user.phone, message, function(){
            return res.redirect('/question/' + answer.question + '#answer-' + answer.id);
          });
        } else {
           return res.redirect('/question/' + answer.question + '#answer-' + answer.id);
        }
      });
      
    });
  },

  downvote: function(req, res) {
    req.query.parentId = req.query.id;
    req.query.parentType = 'answer';
    
    delete req.query.id;
    req.query.user = req.session.user.id;
    
    if (!req.headers.referer) return res.badRequest();

    Answer.findOne({ id: req.query.parentId }, function(err, answer) {
      if (err) return res.serverError();
      if (answer.user === req.query.user) return res.redirect(req.headers.referer);

      Vote.down(req.query, function(err) {
        if (err) return res.serverError();

        res.redirect(req.headers.referer);
      });
    });
  },

  upvote: function(req, res) {
    req.query.parentId = req.query.id;
    req.query.parentType = 'answer';
    
    delete req.query.id;
    req.query.user = req.session.user.id;
    
    if (!req.headers.referer) return res.badRequest();

    Answer.findOne({ id: req.query.parentId }, function(err, answer) {
      if (err) return res.serverError();
      if (answer.user === req.query.user) return res.redirect(req.headers.referer);

      Vote.up(req.query, function(err) {
        if (err) return res.serverError();

        res.redirect(req.headers.referer);
      });
    });
  },

  report: function(req, res) {
    if (!req.query.id) return res.badRequest();

    var repData = {
      parentId: req.query.id,
      parentType: 'answer',
      user: req.session.user.id
    };

    if (!req.headers.referer) return res.badRequest();

    Report.findOne(repData, function(err, report) {
      if (err) return res.serverError();
      if (report) return res.redirect(req.headers.referer);

      Report.create(repData, function(err) {
        if (err) return res.serverError();
        res.redirect(req.headers.referer);
      });
    });
  }

};
