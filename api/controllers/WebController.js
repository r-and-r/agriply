module.exports = {
  about: function(req, res){
    res.view('about', {
      user: req.session.user,
      language: req.session.languagePreference
    });
  },
  
  advocacy: function(req, res){
    res.view('advocacy', {
      user: req.session.user,
      language: req.session.languagePreference
    });
  },
  
  localize: function(req, res){
    req.session.languagePreference = req.params.lang;
    res.redirect(req.headers.referer);
  },
}