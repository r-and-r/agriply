var moment = require('moment');
var qs = require('querystring');

module.exports = {
  post: function(req, res) {
    var redirect = 'default';
    if(req.body.alerts === 'on'){
      User.findOne({ id: req.session.user.id}, function(err, user){
        if(user && user.phone){
          redirect = 'default';
        } else if(user && req.session.user.type !== 3){
          redirect = '/user/update';
        } else {
          redirect = '/user/signup';
        }
      });
    }
    Question.create(req.body, function(err, question) {
      if (err) {
        return res.redirect('/question?error=1');
      }

      if(redirect === 'default'){
        res.redirect('/question/' + question.id);
      } else {
        res.redirect(redirect);
      }
    });
  },
  
  addQuestion: function(req, res){
    if (req.query.landing && req.session.user.userLevel === 3) {
      req.session.user.confirmAnon = true;
      req.session.save(function(err) {
        if (err) return sails.log.error(err);
      });
    }

    var retObj = {
      layout: req.layout,
      user: req.session.user,
      language: req.session.languagePreference
    }

    if (req.query.error){
      retObj['status'] = 'error';
      retObj['error'] = req.__(Errors.generate('question_invalid_arguments'));
    }

    res.view('question', retObj);
  },

  downvote: function(req, res) {
    req.query.parentId = req.query.id;
    req.query.parentType = 'question';
    
    delete req.query.id;
    req.query.user = req.session.user.id;
    
    if (!req.headers.referer) return res.badRequest();

    Question.findOne({ id: req.query.parentId }, function(err, question) {
      if (err) return res.serverError();
      if (question.user === req.query.user) return res.redirect(req.headers.referer);

      Vote.down(req.query, function(err) {
        if (err) return res.serverError();

        res.redirect(req.headers.referer);
      });
    });
  },

  upvote: function(req, res) {
    req.query.parentId = req.query.id;
    req.query.parentType = 'question';
    
    delete req.query.id;
    req.query.user = req.session.user.id;
    
    if (!req.headers.referer) return res.badRequest();

    Question.findOne({ id: req.query.parentId }, function(err, question) {
      if (err) return res.serverError();
      if (question.user === req.query.user) return res.redirect(req.headers.referer);

      Vote.up(req.query, function(err) {
        if (err) return res.serverError();

        res.redirect(req.headers.referer);
      });
    });
  },

  index: function(req, res) {
    var sort = 'createdAt DESC';
    var pagination = { limit: 10, skip: 0 };

    Helper.listQuestions({ uid: req.session.user.id }, pagination, sort, function(err, questions, recentTags) {
      if (err) return res.serverError();

      res.view('index', {
        layout: req.layout,
        questions: questions,
        user: req.session.user,
        tags: recentTags,
        path: req.path,
        language: req.session.languagePreference
      });
    });
  },

  browse: function(req, res) {
    if (req.query.landing && req.session.user.userLevel === 3) {
      req.session.user.confirmAnon = true;
      req.session.save(function(err) {
        if (err) return sails.log.error(err);
      });
    }

    var query = {};
    var keyword = req.query.keyword;
    var tag = req.query.tag;
    
    if (keyword) {
      query = {
        or: [
          { title: { contains: keyword } },
          { content: { contains: keyword } }
        ]
      };
    }

    if (tag) {
      query = {
        tags: { contains: tag },
        uid: req.session.user.id
      };
    }

    var sort = 'createdAt DESC';
    var pageLimit = sails.config.page.limit;
    var page = req.query.page ? req.query.page : 1;
    var pagination = { limit: pageLimit, page: page };

    Helper.listQuestions(query, pagination, sort, function(err, questions, recentTags, maxCount) {
      if (err) return res.serverError();

      delete req.query.page;

      var prevQuery = _.clone(req.query);
      prevQuery.page = parseInt(page) - 1;

      var nextQuery = _.clone(req.query);
      nextQuery.page = parseInt(page) + 1;

      var nav = {
        prev: page > 1,
        next: page * pageLimit < maxCount,
        prevStr: '?' + qs.stringify(prevQuery),
        nextStr: '?' + qs.stringify(nextQuery)
      };

      res.view('browse', {
        layout: req.layout,
        questions: questions,
        user: req.session.user,
        tags: recentTags,
        path: req.path,
        keyword : keyword,
        tag : tag,
        language: req.session.languagePreference,
        nav: nav
      });
    });

  },

  get: function(req, res) {
    Question
    .findOne({ id: req.params.id })
    .populate('user')
    .exec(function(err, question) {
      if (err) return res.serverError();
      if (!question) return res.notFound();

      async.auto({

        comments: function(callback) {
          Comment
          .find({
            parentId: req.params.id,
            parentType: 'question'
          })
          .populate('user')
          .exec(callback);
        },

        votes: function(callback) {
          Vote.sum({
            parentId: req.params.id,
            parentType: 'question'
          }, function(err, votes) {
            question.votes = votes;
            callback(err);
          });
        },

        answers: function(callback) {
          Answer
          .find({
            question: req.params.id
          })
          .populate('user')
          .exec(callback);
        },

        voted: function(callback) {
          var votedValue = 0;
          Vote.findOne({
            parentId: req.params.id,
            parentType: 'question',
            user: req.session.user.id
          }, function(err, vote) {
            if (err) return callback(err);
            if (vote) votedValue = vote.value;

            callback(null, votedValue);
          });
        },

        reported: function(callback) {
          Report.findOne({
            parentId: req.params.id,
            parentType: 'question',
            user: req.session.user.id
          }, function(err, report) {
            if (err) return callback(err);

            callback(null, report !== undefined);
          });
        },

        answerVotes: ['answers', function(callback, result) {
          var tmpAnswers = _.clone(result.answers);
          async.each(tmpAnswers, function(answer, eachCallback) {

            async.auto({

              sum: function(autoCallback) {
                Vote.sum({
                  parentId: answer.id,
                  parentType: 'answer'
                }, function(err, votes) {
                  result.answers = _.each(result.answers, function(innerAnswer) {
                    if (answer.id === innerAnswer.id) innerAnswer.votes = votes;
                  });
                  autoCallback(err);
                });
              },

              voted: ['sum', function(autoCallback) {
                var votedValue = 0;
                Vote.findOne({
                  parentId: answer.id,
                  parentType: 'answer',
                  user: req.session.user.id
                }, function(err, vote) {
                  if (vote) votedValue = vote.value;
                  result.answers = _.each(result.answers, function(innerAnswer) {
                    if (answer.id === innerAnswer.id) innerAnswer.votedValue = votedValue;
                  });

                  autoCallback(err);
                });
              }],

              reports: function(autoCallback) {
                Report.findOne({
                  parentId: answer.id,
                  parentType: 'answer',
                  user: req.session.user.id
                }, function(err, report) {
                  if (report) {
                    result.answers = _.each(result.answers, function(innerAnswer) {
                      if (answer.id === innerAnswer.id) innerAnswer.reported = true;
                    });
                  }

                  autoCallback(err);
                });
              }

            }, function(err, result) {
              eachCallback(err);
            });


          }, function(err) {
            callback(err);
          });
        }]

      }, function(err, result) {
        if (err) return res.serverError();
        question.votedValue = result.voted;
        question.reported = result.reported;
        question.createdAt = moment(question.createdAt).fromNow();

        var hasAnswered = _.any(result.answers, function(answer) {
          return answer.user.id === req.session.user.id;
        });

        _.each(result.answers, function(answer) {
          answer.createdAt = moment(answer.createdAt).fromNow();
        });

        question.tags = question.tags.split(',');

        res.view('questionDetails', {
          layout: req.layout,
          user: req.session.user,
          hasAnswered: hasAnswered,
          question: question,
          answers: result.answers,
          comments: result.comments,
          language: req.session.languagePreference
        });
      });

    });
  },

  report: function(req, res) {
    if (!req.query.id) return res.badRequest();

    var repData = {
      parentId: req.query.id,
      parentType: 'question',
      user: req.session.user.id
    };

    if (!req.headers.referer) return res.badRequest();

    Report.findOne(repData, function(err, report) {
      if (err) return res.serverError();
      if (report) return res.redirect(req.headers.referer);

      Report.create(repData, function(err) {
        if (err) return res.serverError();
        res.redirect(req.headers.referer);
      });
    });
  }
};
