module.exports = {
  getUpdate: function(req, res) {
    var retObj = {
      layout: req.layout,
      user: req.session.user,
      language: req.session.languagePreference
    }
    if(req.headers.referer && req.headers.referer.split('/')[3] === 'question' && req.headers.referer.split('/').length === 4){
      retObj['status'] = 'error';
      retObj['error'] = req.__(Errors.generate('update_phone_profile'));
    }
    res.view('editProfile', retObj);
  },

  userLanding: function(req, res) {
    var action;
    switch (req.query.action) {
      case 'ask':
        if (req.session.user.userLevel< 3) return res.redirect('/question');
        action = 'Ask'; 
        break;
      case 'contribute':
        if (req.session.user.userLevel < 3) return res.redirect('/question/browse');
        action = 'Contribute';
        break;
      default:
        return res.badRequest();
        break;
    }

    res.view('userLanding', {
      layout: req.layout,
      user: req.session.user,
      language: req.session.languagePreference,
      action: action
    });
  },

  update: function(req, res) {
    var retObj = {};
    if (req.body.password && (req.body.password !== req.body.confirmPassword)) {
      delete req.body.password;
      delete req.body.confirmPassword;

      retObj = {
        layout: req.layout,
        status: 'error',
        error: Errors.generate('invalid_confirm'),
        user: req.session.user,
        language: req.session.languagePreference
      };

      return res.view('editProfile', retObj);
    }

    var update = {
      firstName: req.body.firstName ? req.body.firstName : undefined,
      lastName: req.body.lastName ? req.body.lastName : undefined,
      occupation: req.body.occupation ? req.body.occupation : undefined,
      province: req.body.province ? req.body.province : undefined,
      email: req.body.email ? req.body.email : undefined,
      phone: req.body.phone ? req.body.phone : undefined,
      password: req.body.password ? Utils.MD5(req.body.password) : undefined
    };
    
    if(update.password === undefined){
      delete update.password;
    }

    User.update({ id: req.session.user.id }, update, function(err) {
      delete req.body.password;
      delete req.body.confirmPassword;

      if (err) {
        retObj = {
          layout: req.layout,
          status: 'error',
          error: Errors.generate(err),
        };
      } else {
        retObj = {
          layout: req.layout,
          status: 'success',
        };
      }

      User.findOne({ id: req.session.user.id }, function(err, user) {
        if (err) return res.serverError();
        req.session.user = user;
        retObj.user = user;
        retObj.language = req.session.languagePreference;
        
        req.session.save(function() {
          res.view('editProfile', retObj);
        });
      });
    });
  },

  get: function(req, res) {
    User.findOne({ id: req.params.id }, function(err, user) {
      if (err) return res.serverError();
      if (!user) return res.notFound();

      async.auto({
        
        questionVotes: function(autoCallback) {
          Question.find({ user: req.params.id }, function(err, questions) {
            if (err) return autoCallback(err);

            var sum = 0;
            async.eachSeries(questions, function(question, eachCallback) {
              Vote.sum({
                parentId: question.id,
                parentType: 'question'
              }, function(err, s) {
                if (err) return eachCallback(err);
                sum += s;
                eachCallback();
              });
            }, function(err) {
              if (err) return autoCallback(err);
              autoCallback(null, sum);
            });
          });
        },

        answerVotes: function(autoCallback) {
          Answer.find({ user: req.params.id }, function(err, answers) {
            if (err) return autoCallback(err);

            var sum = 0;
            async.eachSeries(answers, function(answer, eachCallback) {
              Vote.sum({
                parentId: answer.id,
                parentType: 'answer'
              }, function(err, s) {
                if (err) return eachCallback(err);
                sum += s;
                eachCallback();
              });
            }, function(err) {
              if (err) return autoCallback(err);
              autoCallback(null, sum);
            });
          });
        },

        questions: function(autoCallback) {
          Helper.listQuestions({
            user: req.params.id,
            uid: req.session.user.id
          }, { limit: sails.config.page.limit, skip: 0 }, 'createdAt DESC', autoCallback);
        }

      }, function(err, result) {
        if (err) return res.serverError();
        user.questionVotes = result.questionVotes;
        user.answerVotes = result.answerVotes;
        
        res.view('profile', {
          layout: req.layout,
          currentUser: user,
          user: req.session.user,
          questions: result.questions[0],
          language: req.session.languagePreference
        });
      });
    });
  },

  signup: function(req, res) {
    var redirect = req.query.redirect ? req.query.redirect : req.headers.referer;

    User.register(req.body, function(err, user) {
      var retObj = {};

      if (err) {
        retObj = {
          layout: req.layout,
          status: 'error',
          error: req.__(Errors.generate('signup')),
          user: req.session.user,
          language: req.session.languagePreference,
        };
        res.view('signup', retObj);
      } else {
        req.session.authenticated = true;
        req.session.user = user;

        retObj = {
          layout: req.layout,
          status: 'success',
          user: req.session.user,
          language: req.session.languagePreference
        };

        req.session.save(function() {
          if (redirect) return res.redirect(redirect);
          else res.view('signup', retObj);
        });
      }

    });
  },
  
  getSignup: function(req, res) {
    if (req.session.authenticated) return res.redirect('/');
    
    var retObj = {
      layout: req.layout,
      user: req.session.user,
      language: req.session.languagePreference
    }
    
    if(req.headers.referer && req.headers.referer.split('/')[3] === 'question' && req.headers.referer.split('/').length === 4){
      retObj['status'] = 'error';
      retObj['error'] = req.__(Errors.generate('update_phone_signup'));
    }
    
    res.view('signup', retObj);
  },
  
  getLogin: function(req, res) {
    if (req.session.authenticated) return res.redirect('/');
    res.view('login', {
      layout: req.layout,
      user: req.session.user,
      language: req.session.languagePreference
    });
  },

  login: function(req, res) {
    var redirect = req.query.redirect ? req.query.redirect : req.headers.referer;

    User.login(req.body.username, req.body.password, function(err, user) {
      var retObj = {};

      if (err) {
        retObj = {
          layout: req.layout,
          status: 'error',
          error: req.__(Errors.generate('login')),
          user: req.session.user,
          language: req.session.languagePreference
        };

        res.view('login', retObj);
      } else {

        req.session.authenticated = true;
        req.session.user = user;

        retObj = {
          layout: req.layout,
          status: 'success',
          user: req.session.user,
          language: req.session.languagePreference
        };

        req.session.save(function() {
          if (redirect) return res.redirect(redirect);
          else res.view('login', retObj);
        });
      }
      
    });
  },

  logout: function(req, res) {
    req.session.destroy(function() {
      res.redirect('/');
    });
  }
};
